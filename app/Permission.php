<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'permission_type', 'user_id', 'store_id'
    ];

    public $timestamps = false;
}
