<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Permission_type;
use App\Permission;
use App\Product;
use App\Store;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::where('level',2)->get();
        $product = Product::all();
        $permission_type = Permission_type::all();
        $permission = Permission::all();

        // $unique = $permission->groupBy('user_id');
        $user_id = Permission::where('user_id',Auth::user()->id)->orderBy('store_id')->get();
        $store_id = $user_id->unique('store_id');

        // dd($store_id);
        $store = Store::all();
        return view('home',compact('data','permission_type','product','permission','store','store_id'));
    }
}
