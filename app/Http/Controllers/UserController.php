<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;

class UserController extends Controller
{
    public function user()
    {
    	User::create([
            [
        		'name' => 'user1',
        		'username' => 'username1',
        		'email' => 'user1@email.com',
        		'level' => 2,
        		'password' => bcrypt('username1'),
            ],
            [
                'name' => 'user2',
                'username' => 'username2',
                'email' => 'user2@email.com',
                'level' => 2,
                'password' => bcrypt('username2'),
            ],
            [
                'name' => 'user3',
                'username' => 'username3',
                'email' => 'user3@email.com',
                'level' => 2,
                'password' => bcrypt('username3'),
            ]
        ]);
    	return redirect('/');
    }

    public function admin()
    {
    	User::create([
    		'name' => 'Administrator',
    		'username' => 'adminMenara',
    		'email' => 'admin@email.com',
    		'level' => 1,
    		'password' => bcrypt('123456789')
	    ]);

	    return redirect('/');
    }

    public function product()
    {
    	Product::create([
    		[
	    		'name' => 'Asus Zenphone',
	    		'desc' => 'Asus Mantep Deh',
	    		'created_at' => \Carbon\Carbon::now('Asia/Jakarta'),
	    		'updated_at' => \Carbon\Carbon::now('Asia/Jakarta'),
    		],
    		[
	    		'name' => 'Oppo',
	    		'desc' => 'Oppo Mantep Juga',
	    		'created_at' => \Carbon\Carbon::now('Asia/Jakarta'),
	    		'updated_at' => \Carbon\Carbon::now('Asia/Jakarta'),
    		],
    		[
	    		'name' => 'Samsung',
	    		'desc' => 'Samsung Gak Kalah Mantep',
	    		'created_at' => \Carbon\Carbon::now('Asia/Jakarta'),
	    		'updated_at' => \Carbon\Carbon::now('Asia/Jakarta'),
    		]
	    ]);

	    return redirect('/');
    }
}
