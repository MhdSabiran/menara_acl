<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission_type;
use App\Permission;
use App\Product;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(Request $request)
    {
    	Permission_type::create($request->all());
    	return redirect('/home');
    }

    public function store(Request $request)
    {
        $permission_type = $request->permission_type;
    	$store = $request->store;
        $count_permissionType = count($request->permission_type);
    	$count_store = count($request->store);

        Permission::where('user_id',$request->user_id)->delete();

        for ($i=0; $i < $count_store; $i++) { 
            for ($j=0; $j < $count_permissionType; $j++) { 
                Permission::create([
                    'permission_type' => $permission_type[$j],
                    'user_id' => $request->user_id,
                    'store_id' => $store[$i],
                ]);
            }
        }

    	return redirect('home');
    }

    public function permission($user_id, $store_id)
    {
        $permission = Permission::where('user_id',$user_id)->where('store_id',$user_id)->get();
        $user = $user_id;
        $product = Product::all();
        return view('permission',compact('permission','user','product'));
    }
}
