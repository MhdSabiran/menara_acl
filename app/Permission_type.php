<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission_type extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;
}
