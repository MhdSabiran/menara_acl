@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Dashboard</div>

	                <div class="panel-body">
	                    @if (session('status'))
	                        <div class="alert alert-success">
	                            {{ session('status') }}
	                        </div>
	                    @endif

	                    <table class="table table-striped">
	                        <thead>
	                            <tr>
	                                <th>No.</th>
	                                <th>Name</th>
	                                <th>Desc</th>
	                                <th>Action</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach ($product as $key => $val)
	                                <tr>
	                                    <td>{{$loop->iteration}}</td>
	                                    <td>{{$val->name}}</td>
	                                    <td>{{$val->desc}}</td>
	                                    <td>
	                                        @foreach ($permission as $element)
	                                            @if ($element->user_id == $user AND $element->permission_type == 1)
	                                                <a href="#" class="btn btn-success btn-xs">Create</a>
	                                            @endif

	                                            @if($element->user_id == $user AND $element->permission_type == 2)
	                                                <a href="#" class="btn btn-primary btn-xs">Edit</a>
	                                            @endif

	                                            @if($element->user_id == $user AND $element->permission_type == 3)
	                                                <a href="#" class="btn btn-info btn-xs">Read</a>
	                                            @endif

	                                            @if($element->user_id == $user AND $element->permission_type == 4)
	                                                <a href="#" class="btn btn-danger btn-xs">Delete</a>
	                                            @endif
	                                        @endforeach
	                                        
	                                    </td>
	                                </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection