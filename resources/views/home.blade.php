@extends('layouts.app')

@section('content')

    @if (Auth::user()->level == 1)

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Dashboard <a href="#" class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#store">Add Store</a> <a href="#" class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#myModal">Add Permission Types</a></div>

                        {{-- Modal Add Permission Types --}}
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ url('permission-type/add') }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label for="name">Name of Permission Types:</label>
                                                <input type="text" class="form-control" id="name" name="name">
                                            </div>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        {{-- Modal Store --}}
                        <div id="store" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ url('store/add') }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label for="name">Name of Store:</label>
                                                <input type="text" class="form-control" id="name" name="name">
                                            </div>
                                            <button type="reset" class="btn btn-danger">Reset</button>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $val)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$val->name}}</td>
                                            <td>{{$val->username}}</td>
                                            <td>{{$val->email}}</td>
                                            <td>
                                                <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#permission_{{$val->id}}">Add Permission</button>

                                                <!-- Modal -->
                                                <div id="permission_{{$val->id}}" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Modal Header</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{ url('permission/store') }}" method="post">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="user_id" value="{{$val->id}}">
                                                                <div class="form-group">
                                                                    <label for="name">Name :</label>
                                                                    <input type="text" class="form-control" value="{{$val->name}}" id="name" readonly>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="name">Store :</label> <br>
                                                                    @foreach ($store as $value)
                                                                        <label>
                                                                            <input type="checkbox" name="store[]" value="{{$value->id}}"> {{$value->name}}
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="name">Permission Type :</label> <br>
                                                                    @foreach ($permission_type as $element)
                                                                        <label>
                                                                            <input type="checkbox" name="permission_type[]" value="{{$element->id}}"> {{$element->name}}
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                                <button type="submit" class="btn btn-success">Submit</button>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                  </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @elseif(Auth::user()->level == 2)

        <div class="container">
            <div class="row">
                @foreach ($store_id as $element)
                    @if ($element->user_id == Auth::user()->id AND $element->store_id == 1)
                        <div class="col-md-4">
                            <div class="jumbotron text-center">
                                <h2>{{\App\Helper::store(1)}}</h2>
                                <p><a href="{{ url('permission/'.Auth::user()->id.'/1') }}" class="btn btn-success btn-xs">Manage Data</a></p> 
                            </div>
                        </div>
                    @endif

                    @if($element->user_id == Auth::user()->id AND $element->store_id == 2)
                        <div class="col-md-4">
                            <div class="jumbotron text-center">
                                <h2>{{\App\Helper::store(2)}}</h2>
                                <p><a href="{{ url('permission/'.Auth::user()->id.'/2') }}" class="btn btn-success btn-xs">Manage Data</a></p> 
                            </div>
                        </div>
                    @endif

                    @if($element->user_id == Auth::user()->id AND $element->store_id == 3)
                        <div class="col-md-4">
                            <div class="jumbotron text-center">
                                <h2>{{\App\Helper::store(3)}}</h2>
                                <p><a href="{{ url('permission/'.Auth::user()->id.'/3') }}" class="btn btn-success btn-xs">Manage Data</a></p> 
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    @endif
@endsection
