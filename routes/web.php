<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['prefix' => 'add'], function () {
	Route::get('/user', 'UserController@user');
	Route::get('/admin', 'UserController@admin');
	Route::get('/product', 'UserController@product');
});

Route::group(['prefix' => 'permission-type'], function () {
	Route::post('/add', 'PermissionController@add');
});

Route::group(['prefix' => 'permission'], function () {
	Route::post('/store', 'PermissionController@store');
	Route::get('/{user_id}/{store_id}', 'PermissionController@permission');
});

Route::group(['prefix' => 'store'], function () {
	Route::post('/add', 'StoreController@add');
});
